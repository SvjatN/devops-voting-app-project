provider "aws" {
  
}

data "aws_availability_zones" "all" {}

# Security Group
resource "aws_security_group" "my_webserver" {
  name        = "WebServer Security Group"
  description = "WebServer Security Group"
 
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#AWS Launch Configuration 
resource "aws_launch_configuration" "voidapp" {
  name = "terraform-launch-config-voidapp"
  image_id = "ami-003634241a8fcdec0"
  instance_type          = "t2.micro"
  key_name = "MyKeyPair"
  security_groups        = [aws_security_group.my_webserver.id]
  user_data = file("user_data.sh")
  lifecycle {
    create_before_destroy = true
  }
  # tag {
  #   key = "Name"
  #   value = "terraform-launch-config-voidapp"
  # }
}
# AWS autoscaling group
resource "aws_autoscaling_group" "voidapp" {
  name = "autoscaling_group_voidapp"
  launch_configuration = aws_launch_configuration.voidapp.id
  desired_capacity = 2
  availability_zones = ["us-west-2c", "us-west-2a", "us-west-2b"]
  #availability_zones = ["${data.aws_availability_zones.all.names}"]
  min_size = 1
  max_size = 3
  load_balancers = ["${aws_elb.voidapp.name}"]
  health_check_type = "ELB"
  tag {
    key = "Name"
    value = "terraform-asg-voidapp"
    propagate_at_launch = true
  }
}
## Auto scaling policies
resource "aws_autoscaling_policy" "voidapp-scale-up" {
    name = "voidapp-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = aws_autoscaling_group.voidapp.name
}
resource "aws_autoscaling_policy" "voidapp-scale-down" {
    name = "voidapp-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = aws_autoscaling_group.voidapp.name
}
## AWS cloudwatch metrics alarm
resource "aws_cloudwatch_metric_alarm" "cpu-high" {
    alarm_name = "cpu-high-voidapp"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "1"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "300"
    statistic = "Average"
    threshold = "50"
    alarm_description = "This metric monitors CPU utilization on ec2 instances"
    alarm_actions = [
        "${aws_autoscaling_policy.voidapp-scale-up.arn}"
    ]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.voidapp.name}"
    }
}
resource "aws_cloudwatch_metric_alarm" "cpu-low" {
    alarm_name = "cpu-low-voidapp"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "1"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "300"
    statistic = "Average"
    threshold = "20"
    alarm_description = "This metric monitors CPU utilization on ec2 instances"
    alarm_actions = [
        "${aws_autoscaling_policy.voidapp-scale-down.arn}"
    ]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.voidapp.name}"
    }
}


resource "aws_elb" "voidapp" {
  name = "terraform-elb-voidapp"
  security_groups = ["${aws_security_group.my_webserver.id}"]
  availability_zones = ["us-west-2c", "us-west-2a", "us-west-2b"]
  
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
   #listener {

  #  lb_port = 443
  #  lb_protocol = "https"
  # instance_port = "80"
  #  instance_protocol = "http"
    #ssl_certificate_id = "arn:aws:iam::230310623363:server-certificate/elastic-beanstalk-x509"
  #}
}